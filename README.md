# Java Thread Example

## Background
This is a classic Computer science problem which examines how Threads are used.
If not careful, the threads can become deadlocked.  

Probably will expand this into some sort of video game implementation

## References

 * [Wikipedia](https://en.wikipedia.org/wiki/Dining_philosophers_problem)
 * [Java Dining Philosophers Tutorial](http://www.baeldung.com/java-dining-philoshophers)
